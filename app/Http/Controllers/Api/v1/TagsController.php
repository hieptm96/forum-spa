<?php

namespace App\Http\Controllers\Api\v1;

use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagsController extends Controller
{
  public function getTags(Request $request)
  {
    $tags = Tag::all();

    return response([
      'status' => 1,
      'message' => 'Success',
      'data' => $tags
    ]);
  }
}
